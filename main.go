package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"gitlab.com/stromy/api.git/src/loadDTB.go"
	"gitlab.com/stromy/api.git/src/middlewares"
	"gitlab.com/stromy/api.git/src/routes"
)

func handleRequests() {
	r := mux.NewRouter()

	r.HandleFunc("/tree/addTree", routes.AddTree)
	r.HandleFunc("/tree/updateTree", routes.UpdateTree)
	r.HandleFunc("/tree/countTrees", routes.CountTrees)
	r.HandleFunc("/tree/addTreePictures", routes.AddTreePictures)
	r.HandleFunc("/tree/addTreeFiles", routes.AddTreeFiles)
	r.HandleFunc("/tree/getAllVlast", routes.GetAllVlast)
	r.HandleFunc("/tree/getTreesAround", routes.GetTreesAround)
	r.HandleFunc("/tree/getById", routes.GetTreeByID)
	r.PathPrefix("/tree/uploads/").Handler(http.StripPrefix("/tree/uploads/", http.FileServer(http.Dir("./uploads/"))))

	r.HandleFunc("/area/kraj", routes.GetKraj)
	r.HandleFunc("/area/okres", routes.GetOkres)
	r.HandleFunc("/area/orp", routes.GetOrp)
	r.HandleFunc("/area/mzchu", routes.GetMzchu)
	r.HandleFunc("/area/vzchu", routes.GetVzchu)

	// .Handler(http.FileServer(http.Dir("./uploads/")))

	r.HandleFunc("/admin/tree/getNTrees", routes.GetTrees)
	r.HandleFunc("/admin/tree/validateTree", routes.ValidateTree)
	r.HandleFunc("/admin/tree/deleteTree", routes.DeleteTree)

	r.HandleFunc("/superAdmin/getAllUsers", routes.GetAllUsers)
	r.HandleFunc("/superAdmin/registerUser", routes.RegisterUser)
	r.HandleFunc("/superAdmin/deleteUser", routes.DeleteUser)

	r.HandleFunc("/admin/export/strom", routes.ExportStrom)
	r.HandleFunc("/admin/export/appStrom", routes.ExportAppStrom)

	// will export JSON with id, name
	http.HandleFunc("/export/kraj", routes.ExportKraj)
	http.HandleFunc("/export/okres", routes.ExportOkres)
	http.HandleFunc("/export/mzchu", routes.ExportMZCHU)
	http.HandleFunc("/export/vzchu", routes.ExportVZCHU)
	http.HandleFunc("/export/ORP", routes.ExportORP)

	r.HandleFunc("/user/login", routes.Login)
	r.HandleFunc("/user/logout", routes.Logout)
	r.HandleFunc("/user/password", routes.ChangePassword)
	// http.HandleFunc("/user/signup", routes.NewUser)

	http.Handle("/tree/", middlewares.CorsMiddleware(r))
	http.Handle("/area/", middlewares.CorsMiddleware(r))
	http.Handle("/user/", middlewares.UserCorsMiddleware(r))
	http.Handle("/admin/", middlewares.AuthenticatedMiddleware(r))
	http.Handle("/superAdmin/", middlewares.SuperAuthenticatedMiddleware(r))

	port := os.Getenv("PORT")
	if port == "" {
		port = ":3000"
	}
	log.Println("STARTING SERVER ON PORT: ", port)

	log.Fatal(http.ListenAndServe(port, nil))
}

func main() {

	// hashedPassword, _ := bcrypt.GenerateFromPassword([]byte("admin 007"), 8)
	// fmt.Println(string(hashedPassword))

	initFlag := flag.Bool("init", false, "do you want to init database from dtb files?")
	flag.Parse()
	init := *initFlag
	if init {
		fmt.Println(" STARTING DATABASE INITIALIZATION ")
		loadDTB.LoadDTB()
	} else {
		handleRequests()
	}
}
