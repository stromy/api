FROM golang:alpine

ADD ./ /go/src/app

WORKDIR /go/src/app

COPY go.mod ./
RUN go mod download

# ENV GOPATH=/go/src/app

ENTRYPOINT ["go", "run", "main.go"]

# FROM node:latest as node

# WORKDIR /home/node/app

# COPY "package*.json" ./
# COPY "tsconfig*.json" ./
# COPY "./src/" "./src/"

# RUN npm install

# RUN ./node_modules/.bin/tsc

# ENTRYPOINT [ "node", "./dist/server.js" ]

# # npm install -g typescript