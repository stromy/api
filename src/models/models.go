package models

type TreeMinimum struct {
	ID        int64  `json:"id"`
	NAME      string `json:"name"`
	TYP_OBJ   string `json:"type"`
	LAT       string `json:"lat"`
	LON       string `json:"lon"`
	OD        string `json:"od"`
	Distance  int    `json:"distance"`
	Validated string `json:"validated"`
}

type TreeRow struct {
	Tree      Tree      `json:"strom"`
	Lokal     Lokal     `json:"lokal"`
	PisemneD  PisemneD  `json:"pisemneD"`
	ObrazoveD ObrazoveD `json:"obrazoveD"`
	Kateg     Kateg     `json:"kateg"`
	Comment   Comment   `json:"comment"`
	Ohro      Ohro      `json:"ohro"`
}

type Tree struct {
	ID     int64  `json:"ID"`
	IDEX   string `json:"IDEX"`
	Name   string `json:"NAME"`
	TypObj string `json:"TYP_OBJ"`
	Datin  string `json:"DATIN"`
	Datak  string `json:"DATAK"`
	Datvy  string `json:"DATVY"`
	Datcs  string `json:"DATCS"`
	Vlast  string `json:"VLAST"`
	Exurl  string `json:"EXURL"`
}

func (T *Tree) FillStruct(
	ID int64, IDEX string, name string, typObj string,
	datin string, datak string, datvy string, datcs string, vlast string, exurl string) {
	T.ID = ID
	T.IDEX = IDEX
	T.Name = name
	T.TypObj = typObj
	T.Datin = datin
	T.Datak = datak
	T.Datvy = datvy
	T.Datcs = datcs
	T.Vlast = vlast
	T.Exurl = exurl
}

type Lokal struct {
	X     string `json:"X"`
	Y     string `json:"Y"`
	Lat   string `json:"LAT"`
	Lon   string `json:"LON"`
	Orp   string `json:"ORP"`
	Kodok string `json:"KODOK"`
	Kraj  string `json:"KRAJ"`
	MZchu string `json:"M_ZCHU"`
	VZchu string `json:"V_ZCHU"`
}

func (L *Lokal) FillStruct(x string, y string, lat string, lon string,
	orp string, kodok string, kraj string, mZchu string, vZchu string) {
	L.X = x
	L.Y = y
	L.Lat = lat
	L.Lon = lon
	L.Orp = orp
	L.Kodok = kodok
	L.Kraj = kraj
	L.MZchu = mZchu
	L.VZchu = vZchu
}

type PisemneD struct {
	Url string `json:"URL"`
}

func (PD *PisemneD) FillStruct(URL string) {
	PD.Url = URL
}

type ObrazoveD struct {
	Url string `json:"URL"`
}

func (OD *ObrazoveD) FillStruct(URL string) {
	OD.Url = URL
}

type Kateg struct {
	Kateg1 string `json:"KATEG1"`
	Kateg2 string `json:"KATEG2"`
	Kateg3 string `json:"KATEG3"`
	Kateg4 string `json:"KATEG4"`
	Kateg5 string `json:"KATEG5"`
}

func (K *Kateg) FillStruct(kateg1 string, kateg2 string, kateg3 string, kateg4 string, kateg5 string) {
	K.Kateg1 = kateg1
	K.Kateg2 = kateg2
	K.Kateg3 = kateg3
	K.Kateg4 = kateg4
	K.Kateg5 = kateg5
}

type Comment struct {
	ComU string `json:"COM_U"`
	ComA string `json:"COM_A"`
}

func (C *Comment) FillStruct(comU string, comA string) {
	C.ComU = comU
	C.ComA = comA
}

type Ohro struct {
	Ohro1 string `json:"OHRO1"`
	Ohro2 string `json:"OHRO2"`
	Ohro3 string `json:"OHRO3"`
	Ohro4 string `json:"OHRO4"`
	Ohro5 string `json:"OHRO5"`
}

func (O *Ohro) FillStruct(ohro1 string, ohro2 string, ohro3 string, ohro4 string, ohro5 string) {
	O.Ohro1 = ohro1
	O.Ohro2 = ohro2
	O.Ohro3 = ohro3
	O.Ohro4 = ohro4
	O.Ohro5 = ohro5
}

type Kraj struct {
	KodKraj string `json:"KOD_KRAJ"`
	KodCz   string `json:"KOD_CZ"`
	Naz     string `json:"NAZ"`
}

type Okres struct {
	KodOkres string `json:"KOD_OKRES"`
	KodLau   string `json:"KOD_LAU"`
	Naz      string `json:"NAZ"`
	KodKraj  string `json:"KOD_KRAJ"`
}

type Orp struct {
	KodOrp   string `json:"KOD_ORP "`
	Naz      string `json:"NAZ"`
	KodOkres string `json:"KOD_OKRES"`
	KodKraj  string `json:"KOD_KRAJ"`
}
type Mzchu struct {
	Kod   string `json:"KOD"`
	Kat   string `json:"KAT"`
	Nazev string `json:"NAZEV"`
}
type Vzchu struct {
	Kod   string `json:"KOD"`
	Kat   string `json:"KAT"`
	Nazev string `json:"NAZEV"`
}

// func PrintTree(treeRow TreeRow) {
// 	fmt.Println(
// 		"TREE:\n",
// 		"\tID", treeRow.Tree.ID, "\n",
// 		"\tIDEX", treeRow.Tree.IDEX, "\n",
// 		"\tNAME", treeRow.Tree.NAME, "\n",
// 		"\tTYP_OBJ", treeRow.Tree.TYP_OBJ, "\n",
// 		"\tDATIN", treeRow.Tree.DATIN, "\n",
// 		"\tDATAK", treeRow.Tree.DATAK, "\n",
// 		"\tDATVY", treeRow.Tree.DATVY, "\n",
// 		"\tVLAST", treeRow.Tree.VLAST, "\n",
// 		"\tEXURL", treeRow.Tree.EXURL, "\n",

// 		"LOKAL:\n",
// 		"\tX", treeRow.Lokal.X, "\n",
// 		"\tY", treeRow.Lokal.Y, "\n",
// 		"\tORP", treeRow.Lokal.ORP, "\n",
// 		"\tKODOK", treeRow.Lokal.KODOK, "\n",
// 		"\tKRAJ", treeRow.Lokal.KRAJ, "\n",
// 		"\tM_ZCHU", treeRow.Lokal.M_ZCHU, "\n",
// 		"\tV_ZCHU", treeRow.Lokal.V_ZCHU, "\n",

// 		"PISEMNE_D:\n",
// 		"\t", treeRow.PisemneD.URL, "\n",

// 		"OBRAZOVE_D:\n",
// 		"\t", treeRow.ObrazoveD.URL, "\n",

// 		"KATEG:\n",
// 		"\tKATEG1", treeRow.Kateg.KATEG1, "\n",
// 		"\tKATEG2", treeRow.Kateg.KATEG2, "\n",
// 		"\tKATEG3", treeRow.Kateg.KATEG3, "\n",
// 		"\tKATEG4", treeRow.Kateg.KATEG4, "\n",
// 		"\tKATEG5", treeRow.Kateg.KATEG5, "\n",

// 		"COMMENT:\n",
// 		"\tCOM_U", treeRow.Comment.COM_U, "\n",
// 		"\tCOM_A", treeRow.Comment.COM_A, "\n",

// 		"OHRO:\n",
// 		"\tOHRO1", treeRow.Ohro.OHRO1, "\n",
// 		"\tOHRO2", treeRow.Ohro.OHRO2, "\n",
// 		"\tOHRO3", treeRow.Ohro.OHRO3, "\n",
// 		"\tOHRO4", treeRow.Ohro.OHRO4, "\n",
// 		"\tOHRO5", treeRow.Ohro.OHRO5, "\n",
// 	)
// }
