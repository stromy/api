package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/stromy/api.git/src/controllers"
	"gitlab.com/stromy/api.git/src/lib"
	"gitlab.com/stromy/api.git/src/models"
)

// GetAllVlast is function, that returns all available owners of data
// should be loaded from database, but for now and for faster processing is hardcoded
func GetAllVlast(w http.ResponseWriter, r *http.Request) {
	vlastList := [5]string{"APPVS", "ZABAGED 2018", "ÚSOP 2018", "NAP 2019", "NPU 2020"}
	vlastString := "["
	for _, vlast := range vlastList {
		vlastString += "\"" + vlast + "\", "
	}
	vlastString = vlastString[:len(vlastString)-2] + "]"
	w.Write([]byte("{\"status\": \"ok\", \"message\":" + vlastString + "}"))
	return
}

// GetTreeByID will get request parameters and handle it
func GetTreeByID(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.Form.Get("id"))
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"wrong id format\"}"))
		return
	}
	getValide := r.Form.Get("getValide")
	var tree models.TreeRow
	if getValide == "true" {
		tree = controllers.TreeByID(id, true)
	} else {
		tree = controllers.TreeByID(id, false)
	}
	treeJSON, err := json.Marshal(tree)

	if err != nil {
		w.Write([]byte("error in formating tree" + err.Error()))
	} else {
		w.Write(treeJSON)
	}
}

// AddTree will handle adding tree by user
func AddTree(w http.ResponseWriter, r *http.Request) {
	var t models.TreeRow
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	LON, err := strconv.ParseFloat(t.Lokal.X, 64)
	if err != nil {
		log.Println(err)
	}
	LAT, err := strconv.ParseFloat(t.Lokal.Y, 64)
	if err != nil {
		log.Println(err)
	}
	X, Y := lib.WGS84toJTSK(LAT, LON)
	t.Lokal.X = fmt.Sprintf("%.2f", X)
	t.Lokal.Y = fmt.Sprintf("%.2f", Y)
	id, err := controllers.AddTreeRow(t, false)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + strconv.Itoa(id) + "\"}"))
}

// AddTree will handle adding tree by user
func UpdateTree(w http.ResponseWriter, r *http.Request) {
	var t models.TreeRow
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	LON, err := strconv.ParseFloat(t.Lokal.Lon, 64)
	if err != nil {
		log.Println(err)
	}
	LAT, err := strconv.ParseFloat(t.Lokal.Lat, 64)
	if err != nil {
		log.Println(err)
	}
	X, Y := lib.WGS84toJTSK(LAT, LON)
	t.Lokal.X = fmt.Sprintf("%.2f", X)
	t.Lokal.Y = fmt.Sprintf("%.2f", Y)
	err = controllers.EditTreeRow(t, false)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + strconv.FormatInt(t.Tree.ID, 10) + "\"}"))
}

func GetTreesAround(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		fmt.Println(err)
	}
	centerF := r.Form.Get("center")
	radius, err := strconv.Atoi(r.Form.Get("radius"))

	center := strings.Split(centerF, ",")
	lon, err1 := strconv.ParseFloat(center[0][1:], 64)
	lat, err2 := strconv.ParseFloat(center[1][:len(center[1])-1], 64)

	if err != nil || err1 != nil || err2 != nil || lon == 0 || lat == 0 || radius == 0 {
		log.Println(err, err1, err2, lon, lat)
		http.Error(w, "cannot parse form", http.StatusBadRequest)
		return
	}

	trees := controllers.GetTreesAround(lon, lat, radius)
	// PROCESS AND SEND
	treesJSON, err := json.Marshal(trees)
	if err != nil {
		log.Println(err)
	}
	fmt.Fprintf(w, string(treesJSON))

}

// GetTrees will read request parameters and handle response
func GetTrees(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	start := r.Form.Get("start")
	n := r.Form.Get("n")
	getValide := r.Form.Get("getValide")
	dateStart := r.Form.Get("dateStart")
	dateEnd := r.Form.Get("dateEnd")
	vlastF := r.Form.Get("vlast")
	typObjF := r.Form.Get("typObj")
	centerF := r.Form.Get("center")
	radius := r.Form.Get("radius")
	krajIDF := r.Form.Get("krajID")
	kodokIDF := r.Form.Get("kodokID")
	orpIDF := r.Form.Get("orpID")
	mZchuIDF := r.Form.Get("mZchuID")
	vZchuIDF := r.Form.Get("vZchuID")

	var vlast []string
	if vlastF != "" {
		if err := json.Unmarshal([]byte(vlastF), &vlast); err != nil {
			log.Println(err)
		}
	}
	var typObj []string
	if typObjF != "" {
		if err := json.Unmarshal([]byte(typObjF), &typObj); err != nil {
			log.Println(err)
		}
	}

	// distance
	var center []string
	if centerF != "" {
		if err := json.Unmarshal([]byte(centerF), &center); err != nil {
			log.Println("[ERROR PARSING DISTANCE]:", err)
		}
	}

	// kraj, kodok, orp, mzchu, vZchu
	var krajID []string
	if krajIDF != "" {
		if err := json.Unmarshal([]byte(krajIDF), &krajID); err != nil {
			log.Println(err)
		}
	}

	var kodokID []string
	if kodokIDF != "" {
		if err := json.Unmarshal([]byte(kodokIDF), &kodokID); err != nil {
			log.Println(err)
		}
	}
	var orpID []string
	if orpIDF != "" {
		if err := json.Unmarshal([]byte(orpIDF), &orpID); err != nil {
			log.Println(err)
		}
	}
	var mZchuID []string
	if mZchuIDF != "" {
		if err := json.Unmarshal([]byte(mZchuIDF), &mZchuID); err != nil {
			log.Println(err)
		}
	}
	var vZchuID []string
	if vZchuIDF != "" {
		if err := json.Unmarshal([]byte(vZchuIDF), &vZchuID); err != nil {
			log.Println(err)
		}
	}

	var treesRows []models.TreeRow
	if getValide == "true" {
		treesRows = controllers.GetTrees(start, n, true, vlast, typObj, center, radius, dateStart, dateEnd, krajID, kodokID, orpID, mZchuID, vZchuID)
	} else {
		treesRows = controllers.GetTrees(start, n, false, vlast, typObj, center, radius, dateStart, dateEnd, krajID, kodokID, orpID, mZchuID, vZchuID)
	}

	// X Y to LAT LON
	for i, tree := range treesRows {
		treesRows[i].Lokal.Lat = ""
		treesRows[i].Lokal.Lon = ""
		if tree.Lokal.X == "" || tree.Lokal.Y == "" {
			continue
		}
		X, errX := strconv.ParseFloat(tree.Lokal.X, 64)
		Y, errY := strconv.ParseFloat(tree.Lokal.Y, 64)
		if errX != nil || errY != nil {
			continue
		}
		Lat, Lon := lib.JTSKtoWGS84(X, Y)
		treesRows[i].Lokal.Lat = fmt.Sprintf("%.6f", Lat)
		treesRows[i].Lokal.Lon = fmt.Sprintf("%.6f", Lon)
	}

	// PROCESS AND SEND
	treesJSON, err := json.Marshal(treesRows)
	if err != nil {
		log.Println(err)
	}
	fmt.Fprintf(w, string(treesJSON))
}

// CountTrees will return count of trees in database to response
func CountTrees(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	getValide := r.Form.Get("getValide")
	var count string
	if getValide == "true" {
		count = controllers.CountTrees(true)
	} else {
		count = controllers.CountTrees(false)
	}

	fmt.Fprintf(w, string(count))
}

// AddTreePictures will get tree from request and handle adding it to database
func AddTreePictures(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(200000)
	if err != nil {
		log.Panic(err)
	}
	formdata := r.MultipartForm

	files := formdata.File["photos"]
	id := formdata.Value["id"][0]
	err = controllers.AddFiles(id, files, "obrazove", "app_strom")
	if err != nil {
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"files added\"}"))
}

// AddTreeFiles will get files from request and handle saving and dtb query
func AddTreeFiles(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(200000)
	if err != nil {
		log.Panic(err)
	}
	formdata := r.MultipartForm

	files := formdata.File["textFiles"]
	id := formdata.Value["id"][0]
	err = controllers.AddFiles(id, files, "pisemne", "app_strom")
	if err != nil {
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"files added\"}"))
}

// ValidateTree process a request to transfer tree from app_strom to strom
func ValidateTree(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.Form.Get("id"))
	if err != nil {
		log.Println(err)
	}
	_, err = controllers.ValidateTree(id)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"tree validated\"}"))
}

// DeleteTree process request of deleting tree (just from app_strom)
func DeleteTree(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	id, err := strconv.Atoi(r.Form.Get("id"))
	if err != nil {
		log.Println(err)
	}
	err = controllers.DeleteTree(id)
	if err != nil {
		log.Println(err)
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"tree deleted\"}"))

}
