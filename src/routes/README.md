# Available routes

* `/tree`
  * `/tree/getById` - will get tree with id from database and return it
    * id
  * `/tree/addTree` - will get tree record from body and save it to databse
  *	`/tree/getNTrees` - get n trees from start
    * start
    * n
  *	`/tree/countTrees` - will return number of trees in databases
  * `/tree/addTreePictures` - for saving pictures to server and database
    * id
    * photos
  * `/tree/addTreeFiles` - for saving text files to server and database
    * id
    * textFiles
  * `/tree/validateTree` - for moving tree from public to final database part
    * id
  * `/tree/deleteTree` - for deleting tree. JUST FROM APP PART OF DTB, NOT VALIDATE PART
    * id


``` json
{
  "name": "addTree",
  "protocolProfileBehavior": {
    "disabledSystemHeaders": {}
  },
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "raw",
      "raw": "{\r\n      \"strom\": {\r\n          \"NAME\":\"dfsfha\",\r\n          \"IDEX\":\"1235\",\r\n          \"TYP_OBJ\":\"alej\",\r\n          \"DATIN\":\"24.7.2020\",\r\n          \"DATAK\":\"\",\r\n          \"DATVY\":\"\",\r\n          \"VLAST\":\"APPVS\",\r\n          \"EXURL\":\"\",\r\n          \"IDNAZ\":\"\",\r\n          \"PRIJEM\":\"\"\r\n          },\r\n      \"lokal\":{\r\n          \"LON\":\"14.484529999999998\",\r\n          \"LAT\":\"50.038551299999995\"\r\n          },\r\n      \"pisemneD\":{\r\n          \"URL\":\"\"\r\n          },\r\n      \"obrazoveD\":{\r\n          \"URL\":\"\"\r\n          },\r\n      \"kateg\":{\r\n          \"KATEG1\":\"3,3,3,0,0,0,0\",\r\n          \"KATEG2\":\"3,1,0\",\r\n          \"KATEG3\":\"0,0\",\r\n          \"KATEG4\":\"0,0,0,0\",\r\n          \"KATEG5\":\"0,0,0,0,0,0\"\r\n          },\r\n      \"comment\":{\r\n          \"COM_U\":\"ffffffff\",\r\n          \"COM_A\":\"\"\r\n          },\r\n      \"ohro\":{\r\n          \"OHRO1\":\"\",\r\n          \"OHRO2\":\"\",\r\n          \"OHRO3\":\"\",\r\n          \"OHRO4\":\"\",\r\n          \"OHRO5\":\"\"\r\n          }\r\n}",
      "options": {
        "raw": {
          "language": "json"
        }
      }
    },
    "url": {
      "raw": "localhost:3000/tree/addTree",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "addTree"
      ]
    }
  },
  "response": []
},
{
  "name": "getTreeById",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "urlencoded",
      "urlencoded": [
        {
          "key": "id",
          "value": "2",
          "type": "text"
        },
        {
          "key": "getValide",
          "value": "false",
          "type": "text"
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/getById",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "getById"
      ]
    }
  },
  "response": []
},
{
  "name": "addTreePictures",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "formdata",
      "formdata": [
        {
          "key": "id",
          "value": "13",
          "type": "text"
        },
        {
          "key": "photos",
          "type": "file",
          "src": [
            "/C:/Users/posekany/Downloads/tree.jpeg",
            "/C:/Users/posekany/Downloads/IMG_0785.jpg"
          ]
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/addTreePictures",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "addTreePictures"
      ]
    }
  },
  "response": []
},
{
  "name": "addTreeFiles",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "formdata",
      "formdata": [
        {
          "key": "id",
          "value": "2",
          "type": "text"
        },
        {
          "key": "textFiles",
          "type": "file",
          "src": [
            "/C:/Users/posekany/Downloads/F6-BP-2020-Simacek-Martin-priloha-2.pdf",
            "/C:/Users/posekany/Downloads/F6-BP-2020-Simacek-Martin-priloha-1.pdf"
          ]
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/addTreeFiles",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "addTreeFiles"
      ]
    }
  },
  "response": []
},
{
  "name": "getNTrees",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "urlencoded",
      "urlencoded": [
        {
          "key": "start",
          "value": "0",
          "type": "text"
        },
        {
          "key": "n",
          "value": "2",
          "type": "text"
        },
        {
          "key": "getValide",
          "value": "false",
          "type": "text"
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/getNTrees",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "getNTrees"
      ]
    }
  },
  "response": []
},
{
  "name": "countTrees",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "urlencoded",
      "urlencoded": [
        {
          "key": "getValide",
          "value": "false",
          "type": "text"
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/countTrees",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "countTrees"
      ]
    }
  },
  "response": []
},
{
  "name": "validateTree",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "urlencoded",
      "urlencoded": [
        {
          "key": "id",
          "value": "3",
          "type": "text"
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/validateTree",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "validateTree"
      ]
    }
  },
  "response": []
},
{
  "name": "deleteTree",
  "request": {
    "method": "POST",
    "header": [],
    "body": {
      "mode": "urlencoded",
      "urlencoded": [
        {
          "key": "id",
          "value": "2",
          "type": "text"
        }
      ]
    },
    "url": {
      "raw": "localhost:3000/tree/deleteTree",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "tree",
        "deleteTree"
      ]
    }
  },
  "response": []
},
{
  "name": "exportCSV",
  "request": {
    "method": "GET",
    "header": [],
    "url": {
      "raw": "localhost:3000/export/kateg",
      "host": [
        "localhost"
      ],
      "port": "3000",
      "path": [
        "export",
        "kateg"
      ]
    }
  },
  "response": []
}
```
