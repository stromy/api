package routes

import (
	"bufio"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"
)

type row1 struct {
	ID   string
	Name string
}
type row2 struct {
	ID     string
	IDKraj string
	Name   string
}
type row4 struct {
	ID      string
	IDKraj  string
	IDOkres string
	Name    string
}
type row3 struct {
	ID        string
	Kategorie string
	Name      string
}

// ExportKraj sends in response array [{ID, Name}]
func ExportKraj(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./dtbDefault/c_kraje.txt")
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	defer file.Close()
	var rows []row1

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}

		// process line
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		var row *row1 = new(row1)
		row.ID = lineArray[0]
		row.Name = lineArray[2]
		rows = append(rows, *row)
		i++
	}
	message, err := json.Marshal(rows)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}

	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + string(message) + "\"}"))
}

// ExportOkres sends in response array [{ID, IDKraj, Name}]
func ExportOkres(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./dtbDefault/c_okresy.txt")
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	defer file.Close()
	var rows []row2

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}

		// process line
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		var row *row2 = new(row2)
		row.ID = lineArray[0]
		row.IDKraj = lineArray[3]
		row.Name = lineArray[2]
		rows = append(rows, *row)
		i++
	}

	message, err := json.Marshal(rows)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}

	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + string(message) + "\"}"))
}

// ExportMZCHU sends in response array [{ID, Kategorie, Name}]
func ExportMZCHU(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./dtbDefault/c_M-ZCHU.txt")
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	defer file.Close()
	var rows []row3

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}

		// process line
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		var row *row3 = new(row3)
		row.ID = lineArray[2]
		row.Kategorie = lineArray[3]
		row.Name = lineArray[4]
		rows = append(rows, *row)
		i++
	}

	message, err := json.Marshal(rows)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}

	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + string(message) + "\"}"))
}

// ExportVZCHU sends in response array [{ID, Kategorie, Name}]
func ExportVZCHU(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./dtbDefault/c_V-ZCHU.txt")
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	defer file.Close()
	var rows []row3

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}

		// process line
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		var row *row3 = new(row3)
		row.ID = lineArray[2]
		row.Kategorie = lineArray[3]
		row.Name = lineArray[4]
		rows = append(rows, *row)
		i++
	}

	message, err := json.Marshal(rows)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}

	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + string(message) + "\"}"))
}

// ExportORP sends in response array [{ID, IDKraj, IDOkres, Name}]
func ExportORP(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./dtbDefault/c_ORP.txt")
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	defer file.Close()
	var rows []row4

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}

		// process line
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		var row *row4 = new(row4)
		row.ID = lineArray[0]
		row.IDOkres = lineArray[3]
		row.IDKraj = lineArray[4]
		row.Name = lineArray[2]
		rows = append(rows, *row)
		i++
	}

	message, err := json.Marshal(rows)
	if err != nil {
		log.Println(err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}

	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + string(message) + "\"}"))
}
