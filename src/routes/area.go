package routes

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/stromy/api.git/src/controllers"
)

func GetKraj(w http.ResponseWriter, r *http.Request) {
	krajArr, err := json.Marshal(controllers.GetKraj())
	if err != nil {
		log.Println(err)
	}
	w.Write(krajArr)
}

func GetOkres(w http.ResponseWriter, r *http.Request) {
	okresArr, err := json.Marshal(controllers.GetOkres())
	if err != nil {
		log.Println(err)
	}
	w.Write(okresArr)
}

func GetOrp(w http.ResponseWriter, r *http.Request) {
	orpArr, err := json.Marshal(controllers.GetOrp())
	if err != nil {
		log.Println(err)
	}
	w.Write(orpArr)
}

func GetMzchu(w http.ResponseWriter, r *http.Request) {
	mzchuArr, err := json.Marshal(controllers.GetMzchu())
	if err != nil {
		log.Println(err)
	}
	w.Write(mzchuArr)

}

func GetVzchu(w http.ResponseWriter, r *http.Request) {
	mzchuArr, err := json.Marshal(controllers.GetMzchu())
	if err != nil {
		log.Println(err)
	}
	w.Write(mzchuArr)

}
