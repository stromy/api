package routes

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/stromy/api.git/src/controllers"
)

var header = []string{"strom_id", "IDEX", "NAME", "TYP_OBJ", "DATIN", "DATAK", "DATVY", "DATCS", "VLAST", "EXURL", "X", "Y", "ORP", "KODOK", "KRAJ", "M_ZCHU", "V_ZCHU", "URL_O", "URL_P", "KATEG1", "KATEG2", "KATEG3", "KATEG4", "KATEG5", "OHRO1", "OHRO2", "OHRO3", "OHRO4", "OHRO5", "COM_U", "COM_A"}

func getFileNameCsv(name string) string {
	year, month, day := time.Now().Date()
	monthStr := fmt.Sprintf("%02d", int(month))
	dayStr := fmt.Sprintf("%02d", day)
	fileName := name + "_" + strconv.Itoa(year)[2:] + monthStr + dayStr + ".csv"
	return fileName
}

// ExportStrom will call ExportCsvStrom, and return file in response
// file structure: "strom_id"; "IDEX"; "NAME"; "TYP_OBJ"; "DATIN"; "DATAK"; "DATVY"; "VLAST"; "EXURL"
func ExportStrom(w http.ResponseWriter, r *http.Request) {
	fileContent, err := controllers.ExportCsvStrom("strom", header)
	if err != nil {
		log.Println(err)
		return
	}

	fileName := getFileNameCsv("strom")

	w.Header().Set("Content-type", "text/csv; charset=utf-8")
	w.Header().Set("Content-Disposition", "attachment; filename=\""+fileName+"\"")
	w.Write([]byte(fileContent))
}

// ExportStrom will call ExportCsvStrom, and return file in response
// file structure: "strom_id"; "IDEX"; "NAME"; "TYP_OBJ"; "DATIN"; "DATAK"; "DATVY"; "VLAST"; "EXURL"
func ExportAppStrom(w http.ResponseWriter, r *http.Request) {
	fileContent, err := controllers.ExportCsvStrom("app_strom", header)
	if err != nil {
		log.Println(err)
		return
	}

	fileName := getFileNameCsv("app_strom")

	w.Header().Set("Content-type", "text/csv; charset=utf-8")
	w.Header().Set("Content-Disposition", "attachment; filename=\""+fileName+"\"")
	w.Write([]byte(fileContent))
}
