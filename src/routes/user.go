package routes

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/stromy/api.git/src/database"

	"github.com/gorilla/sessions"
	"gitlab.com/stromy/api.git/src/controllers"
	"gitlab.com/stromy/api.git/src/models"
)

var store = sessions.NewCookieStore([]byte("9d-vL5er"))

func Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	session, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
	nameEmail := strings.TrimSpace(r.Form.Get("nameEmail"))
	password := strings.TrimSpace(r.Form.Get("password"))

	id, name, role, err := controllers.CheckUser(nameEmail, password)

	if err != nil {
		switch err.Error() {
		case "sql: no rows in result set":
			{
				http.Error(w, "wrong user", http.StatusUnauthorized)
				return
			}
		case "crypto/bcrypt: hashedPassword is not the hash of the given password":
			{
				http.Error(w, "wrong password", http.StatusUnauthorized)
				return
			}
		default:
			{
				http.Error(w, "something went wrong, error saved to logs on server", http.StatusUnauthorized)
				log.Println("[ERROR FROM LOGIN CONTROLLER]", err)
				return
			}
		}
	}

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Values["role"] = role
	session.Values["id"] = id
	session.Save(r, w)
	w.Write(([]byte("{\"status\": \"ok\", \"role\": \"" + role + "\", \"name\": \"" + name + "\"}")))
}

func Logout(w http.ResponseWriter, r *http.Request) {

	session, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Values["id"] = ""
	session.Values["role"] = ""
	session.Save(r, w)
	w.Write(([]byte("{\"status\": \"ok\", \"role\": \"\"}")))
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	session, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	id, ok := session.Values["id"].(string)
	if !ok {
		http.Error(w, "Forbidden, cannot get id", http.StatusForbidden)
		return
	}

	currentPassword := r.Form.Get("currentPassword")
	newPassword := r.Form.Get("newPassword")
	err = controllers.ChangePassword(id, currentPassword, newPassword)

	if err != nil {
		switch err.Error() {
		case "sql: no rows in result set":
			http.Error(w, "unknown name or password", http.StatusUnauthorized)
			return
		case "crypto/bcrypt: hashedPassword is not the hash of the given password":
			http.Error(w, "password doesn't match", http.StatusUnauthorized)
			return
		default:
			http.Error(w, "err.Error()", http.StatusUnauthorized)
			return
		}
		return
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"changed\"}"))
}

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	var user models.User

	user.Name = r.Form.Get("name")
	user.Email = r.Form.Get("email")
	user.Password = r.Form.Get("password")
	user.Phone = r.Form.Get("phone")
	user.Role = r.Form.Get("role")
	ID, err := strconv.ParseInt(r.Form.Get("id"), 10, 64)
	if err != nil {
		log.Println(err)
	}
	user.ID = ID
	if user.ID <= 3 && user.Password == "" {
		http.Error(w, "cannot edit this user", http.StatusBadRequest)
		return
	}
	if user.Name == "" {
		http.Error(w, "no name", http.StatusBadRequest)
		return
	}
	if user.Email == "" {
		http.Error(w, "no email", http.StatusBadRequest)
		return
	}
	if user.Role != "admin" {
		user.Role = "user"
	}

	id, err := controllers.AddUser(user)

	if err != nil {
		errString := err.Error()
		if strings.Contains(errString, "Duplicate entry") {
			if strings.Contains(errString, "name") { // duplicate name
				http.Error(w, "duplicate name", http.StatusBadRequest)
				return
			} else if strings.Contains(errString, "email") { // duplicate email
				http.Error(w, "duplicate email", http.StatusBadRequest)
				return
			}
			http.Error(w, "something went wrong, error saved to logs on server", http.StatusBadRequest)
			log.Println("[ERROR FROM LOGIN CONTROLLER]", err)
			return
		}
	}

	if err != nil {
		fmt.Println("ERROR: ", err)
		w.Write([]byte("{\"status\": \"err\", \"message\": \"" + err.Error() + "\"}"))
		return
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"" + id + "\"}"))

}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	query := "SELECT * FROM user WHERE id>3;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GET ALL USERS]", err)
	}

	var users []models.User
	for rows.Next() {
		var id, name, email, password, phone, role sql.NullString
		err = rows.Scan(&id, &name, &email, &password, &phone, &role)
		if err != nil {
			http.Error(w, "error", http.StatusInternalServerError)
			log.Println(err)
		}
		var user models.User
		user.ID, err = strconv.ParseInt(id.String, 10, 64)
		if err != nil {
			log.Println(err)
		}
		user.Name = name.String
		user.Email = email.String
		user.Phone = phone.String
		user.Role = role.String
		users = append(users, user)
	}

	usersJSON, err := json.Marshal(users)
	if err != nil {
		log.Println(err)
	}
	w.Write(usersJSON)

}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	ID, err := strconv.ParseInt(r.Form.Get("id"), 10, 64)
	if err != nil {
		log.Println(err)
	}
	if ID <= 3 {
		http.Error(w, "cannot remove this user", http.StatusBadRequest)
		return
	}
	query := "DELETE from user where id=" + strconv.FormatInt(ID, 10) + ";"
	_, err = database.DtbConnection.Exec(query)
	if err != nil {
		log.Println("ERROR HERE", err)
	}
	w.Write([]byte("{\"status\": \"ok\", \"message\": \"removed\"}"))
}
