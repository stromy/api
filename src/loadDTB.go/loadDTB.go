package loadDTB

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/stromy/api.git/src/controllers"
	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/models"
)

type ProcessRow struct {
	Line  string
	I     int
	Lokal models.Lokal
}

// THIS IS MESS, but is used only once, hopefully there will be time to rewrite this masterpiece

// LoadDTB will read all required files, get data and save them to database
func LoadDTB() {
	// loadAreas()
	// return
	// open file
	file, err := os.Open("./dtbDefault/t_IDENT.txt")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	// init
	lokal := loadLokal()
	scanner := bufio.NewScanner(file)
	i := 0

	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		if i%100 == 0 {
			fmt.Println(i)
		}
		line := scanner.Text()
		PR := ProcessRow{
			Line:  line,
			I:     i,
			Lokal: *lokal[i-1],
		}
		processLine(PR)

		// processLine(line, i, *lokal[i-1], coords[i-1])
		i++
	}

	if err = scanner.Err(); err != nil {
		log.Println(err)
	}

	// set next auto_increment to 40000 (ids of APPVS will start on 40000)
	_, err = database.DtbConnection.Exec("ALTER TABLE strom AUTO_INCREMENT = 40000;") // <<<<<<<<<<<<<<<<<<<<<<
	if err != nil {
		log.Println(err)
	}
	fmt.Println("ALL DONE, CHECK DATABASE")
}

// this should be run concurrently. It will take line as a text, process it and save to dtb
func processLine(PR ProcessRow) {
	line := PR.Line
	i := PR.I
	lokal := PR.Lokal
	line = strings.Replace(line, "\"", "", -1)

	lineArray := strings.Split(line, ";")

	var tree *models.TreeRow
	tree = new(models.TreeRow)

	tree.Tree.ID = int64(i)
	tree.Tree.IDEX = lineArray[1]
	tree.Tree.Datin = lineArray[3]
	tree.Tree.Datak = lineArray[5]
	tree.Tree.Datvy = lineArray[6]
	tree.Tree.Vlast = lineArray[7]
	tree.Tree.Exurl = lineArray[8]
	tree.Tree.Name = lineArray[9]
	tree.Lokal = lokal

	_, err := controllers.AddTreeRow(*tree, true) // <<<<<<<<<<<<<<<<<<<
	if err != nil {
		fmt.Println(err)
	}
}

func loadLokal() (lokalList []*models.Lokal) {
	// open file
	file, err := os.Open("./dtbDefault/t_LOKAL.txt")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	i := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var lokal *models.Lokal = new(models.Lokal)
		if i == 0 { // skip first row
			i++
			continue
		}
		line := scanner.Text()
		lineArray := strings.Split(line, ";")
		lokal.X = lineArray[1]
		lokal.Y = lineArray[2]
		lokal.Orp = lineArray[3]
		lokal.Kodok = lineArray[4]
		lokal.Kraj = lineArray[5]
		lokal.MZchu = lineArray[6]
		lokal.VZchu = lineArray[7]

		lokalList = append(lokalList, lokal)
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
	return lokalList

}

func loadXY() (lokalXY [][]string) {
	// open file
	file, err := os.Open("./dtbDefault/RVS_CR_20201008.e00")
	// file, err := os.Open("./dtbDefault/t_LOKAL.txt")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	i := 0
	j := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if i == 0 { // skip first row
			i++
			continue
		}
		if i == 1 {
			i = -1
			continue
		}

		line := scanner.Text()
		lineArray := strings.Split(line, "-")

		CX, err := strconv.ParseFloat(lineArray[2], 64)
		if err != nil {
			fmt.Println("[parsing error]", err, lineArray[2])
		}
		CY, err := strconv.ParseFloat(lineArray[1], 64)
		if err != nil {
			fmt.Println("[parsing error]", err, lineArray[1])
		}

		lokalXY = append(lokalXY, []string{fmt.Sprintf("%.1f", CX), fmt.Sprintf("%.1f", CY)})

		if j >= 34309-1 {
			break
		}
		j++
		i++
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
	return lokalXY
}

// >>>>>>>>>>>>>> EXAMPLE OF CONVERSION JSTK-GPS
// _, _, _ := sjtsk2gps.Convert(x, y, 155)
// lokalLine = append(lokalLine, strconv.FormatFloat(lat, 'f', 6, 64))
// lokalLine = append(lokalLine, strconv.FormatFloat(lon, 'f', 6, 64))
// // convert to float64 and from jstk to GPS
// x, err := strconv.ParseFloat(strings.Replace(lineArray[1], ",", ".", -1), 64)
// if err != nil {
// 	log.Println(err)
// }

// // convert to float64 and from jstk to GPS
// y, err := strconv.ParseFloat(strings.Replace(lineArray[2], ",", ".", -1), 64)
// if err != nil {
// 	log.Println(err)
// }
