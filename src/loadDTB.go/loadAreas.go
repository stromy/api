package loadDTB

import (
	"bufio"
	"log"
	"os"
	"strings"

	"gitlab.com/stromy/api.git/src/controllers"
	"gitlab.com/stromy/api.git/src/models"
)

func loadAreas() {
	krajList := loadKraj()
	okresList := loadOkres()
	orpList := loadOrp()
	mzchuList := loadMzchu()
	vzchuList := loadVzchu()
	controllers.AddKrajList(krajList)
	controllers.AddOkresList(okresList)
	controllers.AddOrpList(orpList)
	controllers.AddMzchuList(mzchuList)
	controllers.AddVzchuList(vzchuList)
}

func loadKraj() (krajList []*models.Kraj) {
	krajFile, err := os.Open("./dtbDefault/c_kraje.txt")
	if err != nil {
		log.Println(err)
	}
	defer krajFile.Close()

	scanner := bufio.NewScanner(krajFile)
	i := 0
	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		var kraj *models.Kraj = new(models.Kraj)
		line := scanner.Text()
		lineArray := strings.Split(line, ";")
		kraj.KodKraj = lineArray[0]
		kraj.KodCz = lineArray[1]
		kraj.Naz = lineArray[2]

		krajList = append(krajList, kraj)
	}
	return krajList
}

func loadOkres() (okresList []*models.Okres) {
	okresFile, err := os.Open("./dtbDefault/c_okresy.txt")
	if err != nil {
		log.Println(err)
	}
	defer okresFile.Close()

	scanner := bufio.NewScanner(okresFile)
	i := 0
	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		var okres *models.Okres = new(models.Okres)
		line := scanner.Text()
		lineArray := strings.Split(line, ";")
		okres.KodOkres = lineArray[0]
		okres.KodLau = lineArray[1]
		okres.Naz = lineArray[2]
		okres.KodKraj = lineArray[3]

		okresList = append(okresList, okres)
	}
	return okresList
}

func loadOrp() (orpList []*models.Orp) {
	orpFile, err := os.Open("./dtbDefault/c_ORP.txt")
	if err != nil {
		log.Println(err)
	}
	defer orpFile.Close()

	scanner := bufio.NewScanner(orpFile)
	i := 0
	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		var orp *models.Orp = new(models.Orp)
		line := scanner.Text()
		lineArray := strings.Split(line, ";")
		orp.KodOrp = lineArray[0]
		orp.Naz = lineArray[1]
		orp.KodOkres = lineArray[3]
		orp.KodKraj = lineArray[4]

		orpList = append(orpList, orp)
	}
	return orpList
}

func loadMzchu() (mzchuList []*models.Mzchu) {
	mzchuFile, err := os.Open("./dtbDefault/c_M-ZCHU.txt")
	if err != nil {
		log.Println(err)
	}
	defer mzchuFile.Close()

	scanner := bufio.NewScanner(mzchuFile)
	i := 0
	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		var mzchu *models.Mzchu = new(models.Mzchu)
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		mzchu.Kod = lineArray[2]
		mzchu.Kat = lineArray[3]
		mzchu.Nazev = lineArray[4]
		mzchuList = append(mzchuList, mzchu)
	}
	return mzchuList
}

func loadVzchu() (vzchuList []*models.Vzchu) {
	vzchuFile, err := os.Open("./dtbDefault/c_V-ZCHU.txt")
	if err != nil {
		log.Println(err)
	}
	defer vzchuFile.Close()

	scanner := bufio.NewScanner(vzchuFile)
	i := 0
	for scanner.Scan() {
		if i == 0 {
			i++
			continue
		}
		var vzchu *models.Vzchu = new(models.Vzchu)
		line := scanner.Text()
		lineArray := strings.Split(line, ";")

		vzchu.Kod = lineArray[2]
		vzchu.Kat = lineArray[3]
		vzchu.Nazev = lineArray[4]

		vzchuList = append(vzchuList, vzchu)
	}
	return vzchuList
}
