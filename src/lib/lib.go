package lib

import "math"

var (
	xCorrection = 40
	yCorrection = 45
)

// WGS84toJTSK do exactly how it sounds
func WGS84toJTSK(latitude float64, longitude float64) (float64, float64) {
	// if (latitude < 40) || (latitude > 60) || (longitude < 5) || (longitude > 25) {
	// 	// double[] r = { 0, 0 };
	// 	return 0, 0
	// }
	lat, lon := wgs84toBessel(latitude, longitude)
	X, Y := besseltoJTSK(lat, lon)
	return X - float64(xCorrection), Y - float64(yCorrection) // not perfect, but it works

}

// JTSKtoWGS84 do exactly how it sounds
func JTSKtoWGS84(x float64, y float64) (latitude float64, longitude float64) {
	x = x + float64(xCorrection)
	y = y + float64(yCorrection)
	delta := float64(5)
	latitude = float64(49)
	longitude = float64(14)
	steps := 0
	var v1 float64
	var v2 float64
	var v3 float64
	var v4 float64
	jtskX := float64(0)
	jtskY := float64(0)
	for {
		jtskX, jtskY = WGS84toJTSK(latitude-delta, longitude-delta)
		if jtskX != 0 && jtskY != 0 {
			v1 = distPoints(jtskX, jtskY, x, y)
		} else {
			v1 = 1e32
		}

		jtskX, jtskY = WGS84toJTSK(latitude-delta, longitude+delta)
		if jtskX != 0 && jtskY != 0 {
			v2 = distPoints(jtskX, jtskY, x, y)
		} else {
			v2 = 1e32
		}

		jtskX, jtskY = WGS84toJTSK(latitude+delta, longitude-delta)
		if jtskX != 0 && jtskY != 0 {
			v3 = distPoints(jtskX, jtskY, x, y)
		} else {
			v3 = 1e32
		}

		jtskX, jtskY = WGS84toJTSK(latitude+delta, longitude+delta)
		if jtskX != 0 && jtskY != 0 {
			v4 = distPoints(jtskX, jtskY, x, y)
		} else {
			v4 = 1e32
		}

		if (v1 <= v2) && (v1 <= v3) && (v1 <= v4) {
			latitude = latitude - delta/2
			longitude = longitude - delta/2
		}

		if (v2 <= v1) && (v2 <= v3) && (v2 <= v4) {
			latitude = latitude - delta/2
			longitude = longitude + delta/2
		}

		if (v3 <= v1) && (v3 <= v2) && (v3 <= v4) {
			latitude = latitude + delta/2
			longitude = longitude - delta/2
		}

		if (v4 <= v1) && (v4 <= v2) && (v4 <= v3) {
			latitude = latitude + delta/2
			longitude = longitude + delta/2
		}

		delta *= 0.55
		steps += 4
		if (delta < 0.00001) || (steps > 1000) {
			break
		}
	}

	return latitude, longitude
}

func deg2rad(deg float64) float64 {
	return (deg * math.Pi / 180.0)
}

func rad2deg(rad float64) float64 {
	return (rad / math.Pi * 180.0)
}

func geoCoordsToBLH(x float64, y float64, z float64) (float64, float64, float64) {
	// Bessel's ellipsoid parameters
	a := 6377397.15508
	f1 := 299.152812853
	ab := f1 / (f1 - 1)
	p := math.Sqrt(math.Pow(x, 2) + math.Pow(y, 2))
	e2 := 1 - math.Pow(1-1/f1, 2)
	th := math.Atan(z * ab / p)
	st := math.Sin(th)
	ct := math.Cos(th)
	t := (z + e2*ab*a*math.Pow(st, 3)) / (p - e2*a*math.Pow(ct, 3))

	B := math.Atan(t)
	H := math.Sqrt(1+t*t) * (p - a/math.Sqrt(1+(1-e2)*t*t))
	L := 2 * math.Atan(y/(p+x))

	return B, L, H
}

func blhToGeoCoords(B float64, L float64, H float64) (float64, float64, float64) {
	// WGS-84 ellipsoid parameters
	a := 6378137.0
	f1 := 298.257223563
	e2 := 1 - math.Pow(1-1/f1, 2)
	rho := a / math.Sqrt(1-e2*math.Pow(math.Sin(B), 2))
	x := (rho + H) * math.Cos(B) * math.Cos(L)
	y := (rho + H) * math.Cos(B) * math.Sin(L)
	z := ((1-e2)*rho + H) * math.Sin(B)

	return x, y, z
}

func transformCoords(xs float64, ys float64, zs float64) (float64, float64, float64) {
	// coeficients of transformation from WGS-84 to JTSK
	dx := -570.69
	dy := -85.69
	dz := -462.84 // shift
	wx := 4.99821 / 3600 * math.Pi / 180
	wy := 1.58676 / 3600 * math.Pi / 180
	wz := 5.2611 / 3600 * math.Pi / 180 // rotation
	m := -3.543e-6                      // scale

	xn := dx + (1+m)*(+xs+wz*ys-wy*zs)
	yn := dy + (1+m)*(-wz*xs+ys+wx*zs)
	zn := dz + (1+m)*(+wy*xs-wx*ys+zs)

	return xn, yn, zn
}

func wgs84toBessel(latitude float64, longitude float64) (float64, float64) {
	// altitude := 200
	B := deg2rad(latitude)
	L := deg2rad(longitude)
	// H := float64(altitude)

	// x1, y1, z1 := BLHToGeoCoords(B, L, H)
	// x2, y2, z2 := transformCoords(x1, y1, z1)
	// BLH := geoCoordsToBLH(x2, y2, z2)

	lat := rad2deg(B)
	lon := rad2deg(L)

	return lat, lon
}

func besseltoJTSK(latitude float64, longitude float64) (float64, float64) {
	// a := 6377397.15508
	e := 0.081696831215303
	n := 0.97992470462083
	rho_0 := 12310230.12797036
	sinUQ := 0.863499969506341
	cosUQ := 0.504348889819882
	sinVQ := 0.420215144586493
	cosVQ := 0.907424504992097
	alfa := 1.000597498371542
	k_2 := 1.00685001861538

	B := deg2rad(latitude)
	L := deg2rad(longitude)

	sinB := math.Sin(B)
	t := (1 - e*sinB) / (1 + e*sinB)
	t = math.Pow(1+sinB, 2) / (1 - math.Pow(sinB, 2)) * math.Exp(e*math.Log(t))
	t = k_2 * math.Exp(alfa*math.Log(t))

	sinU := (t - 1) / (t + 1)
	cosU := math.Sqrt(1 - sinU*sinU)
	V := alfa * L
	sinV := math.Sin(V)
	cosV := math.Cos(V)
	cosDV := cosVQ*cosV + sinVQ*sinV
	sinDV := sinVQ*cosV - cosVQ*sinV
	sinS := sinUQ*sinU + cosUQ*cosU*cosDV
	cosS := math.Sqrt(1 - sinS*sinS)
	sinD := sinDV * cosU / cosS
	cosD := math.Sqrt(1 - sinD*sinD)

	eps := n * math.Atan(sinD/cosD)
	rho := rho_0 * math.Exp(-n*math.Log((1+sinS)/cosS))

	return rho * math.Cos(eps), rho * math.Sin(eps)
}

func distPoints(x1 float64, y1 float64, x2 float64, y2 float64) float64 {
	EPS := float64(0.00001)
	dist := hypot(x1-x2, y1-y2)
	if dist < EPS {
		return 0
	}
	return dist
}

func hypot(a float64, b float64) float64 {
	a = math.Abs(a)
	b = math.Abs(b)
	if a < b {
		temp := float64(a)
		a = b
		b = temp
	}

	if a == 0.0 {
		return 0.0
	}

	ba := float64(b / a)
	return a * math.Sqrt(1.0+ba*ba)

}
