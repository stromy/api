package database

import (
	"database/sql"
	"log"
	"os"
)

// DtbConnection global variable serves for communication to database
var DtbConnection *sql.DB

var dbHost, dbDriver, dbUser, dbPass, dbName string

func getEnv() {
	dbHost = os.Getenv("DB_HOST")
	if len(dbHost) == 0 {
		dbHost = "exacum.mendelu.cz:3306"
	}
	dbDriver = os.Getenv("DB_DRIVER")
	if len(dbDriver) == 0 {
		dbDriver = "mysql"
	}
	dbUser = os.Getenv("DB_USER")
	if len(dbUser) == 0 {
		dbUser = "stromy"
	}
	dbPass = os.Getenv("DB_PW")
	if len(dbPass) == 0 {
		dbPass = "4Th,u8U(*]ygE~7G"
	}
	dbName = os.Getenv("DB_NAME")
	if len(dbName) == 0 {
		dbName = "stromy"
	}
}

// - PORT=:8080
// - DB_HOST=176.222.224.212:3306
// - DB_DRIVER=mysql
// - DB_NAME=stromy
// - DB_USER=moja
// - DB_PW=4Th,u8U(*]ygE~7G

func init() {
	getEnv()
	DtbConnection = DtbConn()
	log.Println("Database initialized: ", dbHost)
}

// DtbConn will create connection to database
func DtbConn() (db *sql.DB) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbHost+")/"+dbName)
	if err != nil {
		panic("[DtbConn]: " + err.Error())
	}
	return db
}
