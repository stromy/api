package middlewares

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/sessions"
)

var adminDomain string
var store = sessions.NewCookieStore([]byte("9d-vL5er"))

func init() {
	adminDomain = os.Getenv("ADMIN_ORIGIN")
	if len(adminDomain) == 0 {
		adminDomain = "http://localhost:4200"
		// adminDomain = "http://localhost:8080"
	}
}

func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// fmt.Println(r.Host)
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		if r.Method == "OPTIONS" {
			//handle preflight in here
			w.Write([]byte("OK"))
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func AuthenticatedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, "session")
		if err != nil {
			log.Println(err)
			return
		}

		// check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		_, ok := session.Values["id"].(string)
		if !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		w.Header().Set("Access-Control-Allow-Origin", adminDomain)
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" {
			//handle preflight in here
			w.Write([]byte("OK"))
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func SuperAuthenticatedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, err := store.Get(r, "session")
		if err != nil {
			log.Println(err)
			return
		}

		// check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		// id, ok := session.Values["id"].(string)
		// if !ok {
		// 	http.Error(w, "Forbidden", http.StatusForbidden)
		// 	return
		// }
		role, ok := session.Values["role"].(string)
		if !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		if role != "admin" && role != "superAdmin" {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		w.Header().Set("Access-Control-Allow-Origin", adminDomain)
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" {
			//handle preflight in here
			w.Write([]byte("OK"))
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func UserCorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", adminDomain)
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" {
			//handle preflight in here
			w.Write([]byte("OK"))
			return
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
