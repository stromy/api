package controllers

import (
	"database/sql"
	"log"
	"strconv"
	"strings"

	"gitlab.com/stromy/api.git/src/database"
)

// ExportCsvStrom will get all data from strom table
func ExportCsvStrom(table string, header []string) (string, error) {
	content := strings.Join(header, ";") + "\n"

	query := "SELECT * FROM " + table + ";"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		return "", err
	}

	for rows.Next() {
		var stromID sql.NullInt64
		var NAME, IDEX, typObj, DATIN, DATAK, DATVY, DATCS, VLAST, EXURL, X, Y, ORP, KODOK, KRAJ, M_ZCHU, V_ZCHU, URL_O, URL_P, KATEG1, KATEG2, KATEG3, KATEG4, KATEG5, OHRO1, OHRO2, OHRO3, OHRO4, OHRO5, COM_U, COM_A sql.NullString

		err := rows.Scan(&stromID, &IDEX, &NAME, &typObj, &DATIN, &DATAK, &DATVY, &DATCS, &VLAST, &EXURL, &X, &Y, &ORP, &KODOK, &KRAJ, &M_ZCHU, &V_ZCHU, &URL_O, &URL_P, &KATEG1, &KATEG2, &KATEG3, &KATEG4, &KATEG5, &OHRO1, &OHRO2, &OHRO3, &OHRO4, &OHRO5, &COM_U, &COM_A)
		if err != nil {
			log.Println(err)
		}
		ID := strconv.Itoa(int(stromID.Int64))
		content += ID + ";" + IDEX.String + ";" + NAME.String + ";" + typObj.String + ";" + DATIN.String + ";" + DATAK.String + ";" + DATVY.String + ";" + DATCS.String + ";" + VLAST.String + ";" + EXURL.String + ";" +
			X.String + ";" + Y.String + ";" + ORP.String + ";" + KODOK.String + ";" + KRAJ.String + ";" + M_ZCHU.String + ";" + V_ZCHU.String + ";" +
			URL_O.String + ";" + URL_P.String + ";" +
			KATEG1.String + ";" + KATEG2.String + ";" + KATEG3.String + ";" + KATEG4.String + ";" + KATEG5.String + ";" +
			OHRO1.String + ";" + OHRO2.String + ";" + OHRO3.String + ";" + OHRO4.String + ";" + OHRO5.String + ";" + COM_U.String + ";" + COM_A.String + "\n"
	}
	return content, err
}
