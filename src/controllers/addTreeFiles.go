package controllers

import (
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/stromy/api.git/src/database"
)

// AddFiles will process a row of files, get filenames, save files and save filenames to dtb
func AddFiles(id string, files []*multipart.FileHeader, fileT string, table string) error {
	// TODO handle error if id doesnt exists

	fileNames := getFilesNames(id, len(files))
	for i := range files {
		filename := files[i].Filename
		fileNames[i] = fileNames[i] + filepath.Ext(filename)
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			log.Println(err)
			return err
		}

		out, err := os.Create("./uploads/" + fileT + "/" + fileNames[i])
		defer out.Close()
		if err != nil {
			log.Println(err)
			return err
		}
		_, err = io.Copy(out, file)
		if err != nil {
			log.Println(err)
			return err
		}
	}
	err := addFileToDtb(id, fileNames, fileT, table)
	if err != nil {
		return err
	}
	return nil
}

func getFilesNames(id string, n int) (names []string) {
	year, month, day := time.Now().Date()
	monthStr := fmt.Sprintf("%02d", int(month))
	dayStr := fmt.Sprintf("%02d", day)

	for i := 0; i < n; i++ {
		names = append(names, id+"_"+strconv.Itoa(year)+"-"+monthStr+"-"+dayStr+"_("+strconv.Itoa(i)+")")
	}
	return names
}

// handle saving records of files to database
func addFileToDtb(id string, fileNames []string, fileT string, table string) error {
	fileNamesString := ""
	for i := range fileNames {
		fileNamesString += fileNames[i] + ","
	}
	fileNamesString = strings.TrimSuffix(fileNamesString, ",")

	column := ""

	if fileT == "obrazove" {
		column = "URL_O"
	} else if fileT == "pisemne" {
		column = "URL_P"
	} else {
		log.Println("unsopported ffletype", fileT)
	}

	query := "UPDATE " + table + " SET " + column + "=" + processStrForDB(fileNamesString) +
		" WHERE strom_id = " + processStrForDB(id) + ";"

	_, err := database.DtbConnection.Exec(query)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
