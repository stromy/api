package controllers

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"strconv"

	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/lib"
	"gitlab.com/stromy/api.git/src/models"
)

func GetTreesAround(lon float64, lat float64, radius int) (trees []models.TreeMinimum) {
	center := []string{strconv.FormatFloat(lon, 'f', 6, 64), strconv.FormatFloat(lat, 'f', 6, 64)}
	// COORDINATES BETWEEN
	query1 := processR(center, strconv.Itoa(radius), "strom")
	query2 := processR(center, strconv.Itoa(radius), "app_strom")
	rows1, err := database.DtbConnection.Query(query1)
	defer rows1.Close()
	if err != nil {
		log.Println("[GetTreesAround]: ", err)
	}

	rows2, err := database.DtbConnection.Query(query2)
	defer rows2.Close()
	if err != nil {
		log.Println("[GetTreesAround]: ", err)
	}
	trees = append(trees, getTreeMinimum(rows1, lon, lat, "true")...)
	trees = append(trees, getTreeMinimum(rows2, lon, lat, "false")...)
	return trees

}

// uLon uLat reference position coordinates
func getTreeMinimum(rows *sql.Rows, uLon float64, uLat float64, validated string) (trees []models.TreeMinimum) {
	usersX, usersY := lib.WGS84toJTSK(uLat, uLon)

	for rows.Next() {
		var stromID sql.NullInt64
		var SKIP, NAME, typObj, X, Y, OD sql.NullString
		err := rows.Scan(&stromID, &SKIP, &NAME, &typObj, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &X, &Y, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &OD, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP, &SKIP)
		if err != nil {
			panic("[scanRowsForTrees]: " + err.Error())
		}

		XFloat, err := strconv.ParseFloat(X.String, 10)
		YFloat, err := strconv.ParseFloat(Y.String, 10)
		distance := math.Sqrt(math.Pow(usersX+XFloat, 2) + math.Pow(usersY+YFloat, 2))
		XF, err := strconv.ParseFloat(X.String, 64)
		if err != nil {
			log.Println(err)
		}
		YF, err := strconv.ParseFloat(Y.String, 64)
		if err != nil {
			log.Println(err)
		}
		LAT, LON := lib.JTSKtoWGS84(-XF, -YF)

		var treeMin models.TreeMinimum
		treeMin.ID = stromID.Int64
		treeMin.NAME = NAME.String
		treeMin.TYP_OBJ = typObj.String
		treeMin.LAT = fmt.Sprintf("%f", LAT)
		treeMin.LON = fmt.Sprintf("%f", LON)
		treeMin.OD = OD.String
		treeMin.Distance = int(distance)
		treeMin.Validated = validated
		trees = append(trees, treeMin)
	}
	return trees
}

func processR(center []string, radius string, tableName string) string {
	LON, err := strconv.ParseFloat(center[0], 64)
	if err != nil {
		return ""
	}
	LAT, err := strconv.ParseFloat(center[1], 64)
	if err != nil {
		return ""
	}

	X, Y := lib.WGS84toJTSK(LAT, LON)
	stringX := fmt.Sprintf("%f", X)
	stringY := fmt.Sprintf("%f", Y)

	if X == 0 || Y == 0 || radius == "" {
		return ""
	}
	// (a-b)^2 but in database, numbers are saved as - numbers, thats why +
	return "SELECT * FROM " + tableName +
		" WHERE SQRT( POW(" + stringX + " + " + tableName + ".X, 2) + POW(" + stringY + "+" + tableName + ".Y, 2) ) <= " + radius + "" +
		" LIMIT 0, 1000;"

	// return "WHERE " + tablePrefix + "strom.strom_id IN (SELECT " + tablePrefix + "lokal.strom_id FROM " + tablePrefix + "lokal" +
	// 	" INNER JOIN " + tablePrefix + "strom ON " + tablePrefix + "strom.strom_id = " + tablePrefix + "lokal.strom_id" +
	// 	" WHERE SQRT( POW(" + fmt.Sprintf("%.4f", X) + " - " + tablePrefix + "lokal.X, 2) + POW(" + fmt.Sprintf("%.4f", Y) + "-" + tablePrefix + "lokal.Y, 2) ) <= " + fmt.Sprintf("%.4f", r) + ")"
}
