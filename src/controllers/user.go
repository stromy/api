package controllers

import (
	"fmt"
	"strconv"

	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/models"
	"golang.org/x/crypto/bcrypt"
)

// AddUser add given user to database
func AddUser(user models.User) (id string, err error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 8)
	if err != nil {
		return "", err
	}

	query := ""
	if user.ID > 3 {
		query = "UPDATE user SET name=" + processStrForDB(user.Name) +
			", email=" + processStrForDB(user.Email)
		if user.Password != "" { // change password only if filled
			query += ", password=" + processStrForDB(string(hashedPassword))
		}
		query += ", phone=" + processStrForDB(user.Phone) +
			", role=" + processStrForDB(user.Role) +
			" WHERE id=" + strconv.FormatInt(user.ID, 10) + ";"
	} else {
		query = "INSERT INTO user(name, email, password, phone, role) VALUES (" +
			processStrForDB(user.Name) + ", " +
			processStrForDB(user.Email) + ", " +
			processStrForDB(string(hashedPassword)) + ", " +
			processStrForDB(user.Phone) + ", " +
			processStrForDB(user.Role) + ")"
	}

	fmt.Println(query)

	selDB, err := database.DtbConnection.Exec(query)
	if err != nil {
		fmt.Println("ERROR HERE", err)
		return "", err
	}
	newID, err := selDB.LastInsertId()
	if err != nil {
		return "", err
	}
	return strconv.Itoa(int(newID)), nil
}

// CheckUser will try to find a user by email or name, then check password and return id, name, role and err
// err usually is one of these two:
//	sql: no rows in result set
//	crypto/bcrypt: hashedPassword is not the hash of the given password
func CheckUser(nameEmail string, password string) (id string, name string, role string, err error) {

	queryEmail := "SELECT id, name, password, role FROM user WHERE email='" + nameEmail + "';"
	queryName := "SELECT id, name, password, role FROM user WHERE name='" + nameEmail + "';"
	var hashedPW string
	row := database.DtbConnection.QueryRow(queryEmail)
	err = row.Scan(&id, &name, &hashedPW, &role)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			row = database.DtbConnection.QueryRow(queryName)
			err2 := row.Scan(&id, &name, &hashedPW, &role)
			if err2 != nil {
				return "", "", "", err2
			}
		} else {
			return "", "", "", err
		}
	}

	err = bcrypt.CompareHashAndPassword([]byte(hashedPW), []byte(password))
	if err != nil {
		return "", "", "", err
	} else {
		return id, name, role, nil
	}
}

//
func ChangePassword(id string, password string, newPassword string) (err error) {
	newHashedPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), 8)
	if err != nil {
		return err
	}

	var query, pwQuery string
	query = "SELECT password FROM user WHERE id='" + id + "';"
	pwQuery = "UPDATE user SET password='" + string(newHashedPassword) + "' WHERE id='" + id + "';"
	
	var hashedPW string
	row := database.DtbConnection.QueryRow(query)

	err = row.Scan(&hashedPW)
	if err != nil {
		return err
	}

	err = bcrypt.CompareHashAndPassword([]byte(hashedPW), []byte(password))
	if err != nil {
		return err
	}

	_, err = database.DtbConnection.Query(pwQuery)
	if err != nil {
		return err
	}
	return nil

}
