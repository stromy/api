package controllers

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/models"
)

// AddKrajList will add list to database
func AddKrajList(krajList []*models.Kraj) {
	fmt.Println("STARTING KRAJ")
	for _, kraj := range krajList {
		query := "INSERT INTO kraj" +
			"(KOD_KRAJ, KOD_CZ, NAZ) VALUES (" +
			processStrForDB(kraj.KodKraj) + ", " +
			processStrForDB(kraj.KodCz) + ", " +
			processStrForDB(kraj.Naz) + ");"
		_, err := database.DtbConnection.Exec(query)
		if err != nil {
			fmt.Println("areas kraj ERROR: ", err)
			return
		}
	}
}

// AddKrajList will add list to database
func AddOkresList(okresyList []*models.Okres) {
	fmt.Println("STARTING OKRES")
	for _, okres := range okresyList {
		query := "INSERT INTO okres" +
			"(KOD_OKRES, KOD_LAU, NAZ, KOD_KRAJ) VALUES (" +
			processStrForDB(okres.KodOkres) + ", " +
			processStrForDB(okres.KodLau) + ", " +
			processStrForDB(okres.Naz) + ", " +
			processStrForDB(okres.KodKraj) + ");"
		_, err := database.DtbConnection.Exec(query)
		if err != nil {
			fmt.Println("areas ERROR: ", err)
			return
		}
	}
}

// AddKrajList will add list to database
func AddOrpList(orpList []*models.Orp) {
	fmt.Println("STARTING ORP")
	for _, orp := range orpList {
		query := "INSERT INTO orp" +
			"(KOD_ORP, NAZ, KOD_OKRES, KOD_KRAJ) VALUES (" +
			processStrForDB(orp.KodOrp) + ", " +
			processStrForDB(orp.Naz) + ", " +
			processStrForDB(orp.KodOkres) + ", " +
			processStrForDB(orp.KodKraj) + ");"
		_, err := database.DtbConnection.Exec(query)
		if err != nil {
			fmt.Println("areas orp ERROR: ", err)
			return
		}
	}
}

// AddKrajList will add list to database
func AddMzchuList(mzchuList []*models.Mzchu) {
	fmt.Println("STARTING MZCHU")
	for _, mzchu := range mzchuList {
		query := "INSERT INTO mzchu" +
			"(KOD, KAT, NAZEV) VALUES (" +
			processStrForDB(mzchu.Kod) + ", " +
			processStrForDB(mzchu.Kat) + ", " +
			processStrForDB(mzchu.Nazev) + ");"
		_, err := database.DtbConnection.Exec(query)
		if err != nil {
			fmt.Println("areas mzchu ERROR: ", err)
			return
		}
	}
}

// AddKrajList will add list to database
func AddVzchuList(vzchuList []*models.Vzchu) {
	fmt.Println("STARTING VZCHU")
	for _, vzchu := range vzchuList {
		query := "INSERT INTO vzchu" +
			"(KOD, KAT, NAZEV) VALUES (" +
			processStrForDB(vzchu.Kod) + ", " +
			processStrForDB(vzchu.Kat) + ", " +
			processStrForDB(vzchu.Nazev) + ");"
		_, err := database.DtbConnection.Exec(query)
		if err != nil {
			fmt.Println("areas vzchu ERROR: ", err)
			return
		}
	}
}

func GetKraj() (krajArr []models.Kraj) {
	query := "SELECT * FROM kraj;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetKraj]: ", err)
	}
	for rows.Next() {
		var KOD_KRAJ sql.NullInt64
		var KOD_CZ, NAZ sql.NullString
		err := rows.Scan(&KOD_KRAJ, &KOD_CZ, &NAZ)
		if err != nil {
			panic("[KRAJ scanRowsForTrees]: " + err.Error())
		}
		var kraj models.Kraj
		kraj.KodKraj = strconv.FormatInt(KOD_KRAJ.Int64, 10)
		kraj.KodCz = KOD_CZ.String
		kraj.Naz = NAZ.String
		krajArr = append(krajArr, kraj)
	}
	return krajArr
}

func GetOkres() (okresArr []models.Okres) {
	query := "SELECT * FROM okres;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetKraj]: ", err)
	}
	for rows.Next() {
		var KOD_OKRES, KOD_KRAJ sql.NullInt64
		var KOD_LAU, NAZ sql.NullString
		err := rows.Scan(&KOD_OKRES, &KOD_LAU, &NAZ, &KOD_KRAJ)
		if err != nil {
			panic("[KRAJ scanRowsForTrees]: " + err.Error())
		}
		var okres models.Okres
		okres.KodOkres = strconv.FormatInt(KOD_OKRES.Int64, 10)
		okres.KodKraj = strconv.FormatInt(KOD_KRAJ.Int64, 10)
		okres.KodLau = KOD_LAU.String
		okres.Naz = NAZ.String

		okresArr = append(okresArr, okres)
	}
	return okresArr
}

func GetOrp() (orpArr []models.Orp) {
	query := "SELECT * FROM orp;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetKraj]: ", err)
	}
	for rows.Next() {
		var KOD_ORP, KOD_OKRES, KOD_KRAJ sql.NullInt64
		var NAZ sql.NullString
		err := rows.Scan(&KOD_ORP, &NAZ, &KOD_OKRES, &KOD_KRAJ)
		if err != nil {
			panic("[KRAJ scanRowsForTrees]: " + err.Error())
		}
		var orp models.Orp
		orp.KodOrp = strconv.FormatInt(KOD_ORP.Int64, 10)
		orp.KodOkres = strconv.FormatInt(KOD_OKRES.Int64, 10)
		orp.KodKraj = strconv.FormatInt(KOD_KRAJ.Int64, 10)
		orp.Naz = NAZ.String
		orpArr = append(orpArr, orp)
	}
	return orpArr
}

func GetMzchu() (mzchuArr []models.Mzchu) {
	query := "SELECT * FROM mzchu;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetKraj]: ", err)
	}
	for rows.Next() {
		var KOD sql.NullInt64
		var KAT, NAZEV sql.NullString
		err := rows.Scan(&KOD, &KAT, &NAZEV)
		if err != nil {
			panic("[KRAJ scanRowsForTrees]: " + err.Error())
		}
		var mzchu models.Mzchu
		mzchu.Kod = strconv.FormatInt(KOD.Int64, 10)
		mzchu.Kat = KAT.String
		mzchu.Nazev = NAZEV.String
		mzchuArr = append(mzchuArr, mzchu)
	}
	return mzchuArr

}
func GetVzchu() (vzchuArr []models.Vzchu) {
	query := "SELECT * FROM vzchu;"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetKraj]: ", err)
	}
	for rows.Next() {
		var KOD sql.NullInt64
		var KAT, NAZEV sql.NullString
		err := rows.Scan(&KOD, &KAT, &NAZEV)
		if err != nil {
			panic("[KRAJ scanRowsForTrees]: " + err.Error())
		}
		var vzchu models.Vzchu
		vzchu.Kod = strconv.FormatInt(KOD.Int64, 10)
		vzchu.Kat = KAT.String
		vzchu.Nazev = NAZEV.String
		vzchuArr = append(vzchuArr, vzchu)
	}
	return vzchuArr
}
