package controllers

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/models"
)

// AddTreeRow takes a treeRow and save it to database
// returns id of created tree
func AddTreeRow(treeRow models.TreeRow, finalTree bool) (id int, err error) {
	// selDB, err := db.Query("SELECT * FROM strom WHERE strom_id=?", id)
	ID, err := addTree(treeRow, finalTree)
	if err != nil {
		return 0, err
	}

	IDInt, err := strconv.Atoi(ID)
	if err != nil {
		return 0, err
	}

	return IDInt, nil
}

func EditTreeRow(treeRow models.TreeRow, finalTree bool) (err error) {
	err = editTree(treeRow, finalTree)
	if err != nil {
		return err
	}
	return nil
}

func editTree(tree models.TreeRow, finalTree bool) error {
	ID := strconv.FormatInt(tree.Tree.ID, 10)
	if ID == "0" {
		return errors.New("[controller - Edit tree]: no id provided")
	}

	// set X and Y

	var tableName string
	if finalTree {
		tableName = "strom"
	} else {
		tableName = "app_strom"
	}
	query := "UPDATE " + tableName +
		" set IDEX = " + processIntForDB(tree.Tree.IDEX) +
		",NAME = " + processStrForDB(tree.Tree.Name) +
		",TYP_OBJ = " + processStrForDB(tree.Tree.TypObj) +
		",DATIN = " + processDateForDB(tree.Tree.Datin) +
		",DATAK = " + processDateForDB(tree.Tree.Datak) +
		",DATVY = " + processDateForDB(tree.Tree.Datvy) +
		",VLAST = " + processStrForDB(tree.Tree.Vlast) +
		",EXURL = " + processStrForDB(tree.Tree.Exurl) +
		",X = " + processStrForDB("-"+tree.Lokal.X) +
		",Y = " + processStrForDB("-"+tree.Lokal.Y) +
		",ORP = " + processStrForDB(tree.Lokal.Orp) +
		",KODOK = " + processStrForDB(tree.Lokal.Kodok) +
		",KRAJ = " + processStrForDB(tree.Lokal.Kraj) +
		",M_ZCHU = " + processStrForDB(tree.Lokal.MZchu) +
		",V_ZCHU = " + processStrForDB(tree.Lokal.VZchu) +
		",URL_P = " + processStrForDB(tree.PisemneD.Url) +
		",URL_O = " + processStrForDB(tree.ObrazoveD.Url) +
		",KATEG1 = " + processStrForDB(tree.Kateg.Kateg1) +
		",KATEG2 = " + processStrForDB(tree.Kateg.Kateg2) +
		",KATEG3 = " + processStrForDB(tree.Kateg.Kateg3) +
		",KATEG4 = " + processStrForDB(tree.Kateg.Kateg4) +
		",KATEG5 = " + processStrForDB(tree.Kateg.Kateg5) +
		",COM_A = " + processStrForDB(tree.Comment.ComA) +
		",COM_U = " + processStrForDB(tree.Comment.ComU) +
		",OHRO1 = " + processStrForDB(tree.Ohro.Ohro1) +
		",OHRO2 = " + processStrForDB(tree.Ohro.Ohro2) +
		",OHRO3 = " + processStrForDB(tree.Ohro.Ohro3) +
		",OHRO4 = " + processStrForDB(tree.Ohro.Ohro4) +
		",OHRO5 = " + processStrForDB(tree.Ohro.Ohro5) +
		" WHERE strom_id=" + ID + ";"
	_, err := database.DtbConnection.Exec(query)
	return err
}

func addTree(tree models.TreeRow, finalTree bool) (string, error) {
	ID := strconv.FormatInt(tree.Tree.ID, 10)
	if ID == "0" {
		ID = "NULL"
	}

	var tableName string
	if finalTree {
		tableName = "strom"
	} else {
		tableName = "app_strom"
	}

	if tree.Lokal.X[0] != '-' {
		tree.Lokal.X = "-" + tree.Lokal.X
	}
	if tree.Lokal.Y[0] != '-' {
		tree.Lokal.Y = "-" + tree.Lokal.Y
	}

	query := "INSERT INTO " + tableName +
		" (strom_id, IDEX, NAME, TYP_OBJ, DATIN, DATAK, DATVY, DATCS, VLAST, EXURL," +
		"X, Y, ORP, KODOK, KRAJ, M_ZCHU, V_ZCHU," +
		"URL_P, URL_O," +
		"KATEG1, KATEG2, KATEG3, KATEG4, KATEG5," +
		"COM_A, COM_U," +
		"OHRO1, OHRO2, OHRO3, OHRO4, OHRO5" +
		") VALUES (" + ID + ", " +
		processStrForDB(tree.Tree.IDEX) + ", " + processStrForDB(tree.Tree.Name) + ", " +
		processStrForDB(tree.Tree.TypObj) + ", " + processDateForDB(tree.Tree.Datin) + ", " +
		processDateForDB(tree.Tree.Datak) + ", " + processDateForDB(tree.Tree.Datvy) + ", " + processDateForDB(tree.Tree.Datcs) + ", " +
		processStrForDB(tree.Tree.Vlast) + ", " + processStrForDB(tree.Tree.Exurl) + ", " +

		processStrForDB(tree.Lokal.X) + ", " + processStrForDB(tree.Lokal.Y) + ", " +
		processStrForDB(tree.Lokal.Orp) + ", " + processStrForDB(tree.Lokal.Kodok) + ", " +
		processStrForDB(tree.Lokal.Kraj) + ", " + processStrForDB(tree.Lokal.MZchu) + ", " +
		processStrForDB(tree.Lokal.VZchu) + ", " +

		processStrForDB(tree.PisemneD.Url) + ", " + processStrForDB(tree.ObrazoveD.Url) + ", " +

		processStrForDB(tree.Kateg.Kateg1) + "," +
		processStrForDB(tree.Kateg.Kateg2) + "," + processStrForDB(tree.Kateg.Kateg3) + "," +
		processStrForDB(tree.Kateg.Kateg4) + "," + processStrForDB(tree.Kateg.Kateg5) + "," +

		processStrForDB(tree.Comment.ComA) + ", " + processStrForDB(tree.Comment.ComU) + "," +

		processStrForDB(tree.Ohro.Ohro1) + ", " +
		processStrForDB(tree.Ohro.Ohro2) + ", " + processStrForDB(tree.Ohro.Ohro3) + ", " +
		processStrForDB(tree.Ohro.Ohro4) + ", " + processStrForDB(tree.Ohro.Ohro5) + ");"
	selDB, err := database.DtbConnection.Exec(query)
	// TODO handle duplica error
	// 2020/09/14 16:38:45 http: panic serving [::1]:56237: Error 1062: Duplicate entry 'a4gfjjffjjfgjfha' for key 'NAME'
	if err != nil {
		return "", err
	}
	if ID == "NULL" {
		ID64, err := selDB.LastInsertId()
		if err != nil {
			return "", err
		}
		ID = strconv.FormatInt(ID64, 10)
	}
	return ID, nil
}

func processIntForDB(n string) string {
	num, err := strconv.Atoi(n)
	if err != nil {
		return "\"" + strconv.Itoa(num) + "\""
	}
	return "\"0\""

}

func processDateForDB(dat string) string {
	dat = strings.TrimSpace(dat)
	if dat != "" {
		return "\"" + dat + "\""
	}
	return "NULL"
}

func processStrForDB(str string) string {
	if str == "" {
		return "\"\""
	}
	return "\"" + str + "\""
}
