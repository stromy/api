// func getLokal(id string, getValide bool) (lokal models.Lokal) {
// 	var tableName string
// 	if getValide {
// 		tableName = "lokal"
// 	} else {
// 		tableName = "app_lokal"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"
// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic("[getLokal]: " + err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var X, Y, ORP, KODOK, KRAJ, mZchu, vZchu sql.NullString
// 		err = rows.Scan(&stromID, &X, &Y, &ORP, &KODOK, &KRAJ, &mZchu, &vZchu)
// 		if err != nil {
// 			panic("[getLokal]: " + err.Error())
// 		}
// 		lokal.X = X.String
// 		lokal.Y = Y.String
// 		lokal.ORP = ORP.String
// 		lokal.KODOK = KODOK.String
// 		lokal.KRAJ = KRAJ.String
// 		lokal.M_ZCHU = mZchu.String
// 		lokal.V_ZCHU = vZchu.String
// 		return lokal
// 	}
// 	return lokal
// }

// func getPD(id string, getValide bool) (pd models.PisemneD) {
// 	var tableName string
// 	if getValide {
// 		tableName = "pisemne_d"
// 	} else {
// 		tableName = "app_pisemne_d"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"
// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic(err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var URL sql.NullString
// 		err = rows.Scan(&stromID, &URL)
// 		if err != nil {
// 			panic("[getPD]: " + err.Error())
// 		}
// 		pd.URL = URL.String
// 		return pd
// 	}
// 	return pd
// }

// func getOD(id string, getValide bool) (od models.ObrazoveD) {
// 	var tableName string
// 	if getValide {
// 		tableName = "obrazove_d"
// 	} else {
// 		tableName = "app_obrazove_d"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"

// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic("[getOD]: " + err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var URL sql.NullString
// 		err = rows.Scan(&stromID, &URL)
// 		if err != nil {
// 			panic("[getOD]: " + err.Error())
// 		}
// 		od.URL = URL.String
// 		return od
// 	}
// 	return od

// }

// func getKateg(id string, getValide bool) (kateg models.Kateg) {
// 	var tableName string
// 	if getValide {
// 		tableName = "kateg"
// 	} else {
// 		tableName = "app_kateg"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"

// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic(err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var KATEG1, KATEG2, KATEG3, KATEG4, KATEG5 sql.NullString
// 		err = rows.Scan(&stromID, &KATEG1, &KATEG2, &KATEG3, &KATEG4, &KATEG5)
// 		if err != nil {
// 			panic("[getKateg]: " + err.Error())
// 		}
// 		kateg.KATEG1 = KATEG1.String
// 		kateg.KATEG2 = KATEG2.String
// 		kateg.KATEG3 = KATEG3.String
// 		kateg.KATEG4 = KATEG4.String
// 		kateg.KATEG5 = KATEG5.String
// 		return kateg
// 	}
// 	return kateg
// }

// func getComment(id string, getValide bool) (comment models.Comment) {
// 	var tableName string
// 	if getValide {
// 		tableName = "comment"
// 	} else {
// 		tableName = "app_comment"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"
// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic("[getComment]: " + err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var comU, comA sql.NullString
// 		err = rows.Scan(&stromID, &comU, &comA)
// 		if err != nil {
// 			panic("[getComment]: " + err.Error())
// 		}
// 		comment.COM_U = comU.String
// 		comment.COM_A = comA.String
// 		return comment
// 	}
// 	return comment
// }

// func getOhro(id string, getValide bool) (ohro models.Ohro) {
// 	var tableName string
// 	if getValide {
// 		tableName = "ohro"
// 	} else {
// 		tableName = "app_ohro"
// 	}
// 	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + id + ";"
// 	rows, err := database.DtbConnection.Query(query)
// 	defer rows.Close()
// 	if err != nil {
// 		panic("[getOhro]: " + err.Error())
// 	}
// 	for rows.Next() {
// 		var stromID sql.NullInt64
// 		var OHRO1, OHRO2, OHRO3, OHRO4, OHRO5 sql.NullString
// 		err = rows.Scan(&stromID, &OHRO1, &OHRO2, &OHRO3, &OHRO4, &OHRO5)
// 		if err != nil {
// 			panic("[getOhro]: " + err.Error())
// 		}
// 		ohro.OHRO1 = OHRO1.String
// 		ohro.OHRO2 = OHRO2.String
// 		ohro.OHRO3 = OHRO3.String
// 		ohro.OHRO4 = OHRO4.String
// 		ohro.OHRO5 = OHRO5.String
// 		return ohro
// 	}
// 	return ohro
// }
