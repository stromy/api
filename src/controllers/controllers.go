package controllers

import (
	"log"
	"strconv"

	"gitlab.com/stromy/api.git/src/database"
)

// CountTrees will get and return count of trees from database
func CountTrees(getValide bool) string {
	var tableName string
	if getValide {
		tableName = "strom"
	} else {
		tableName = "app_strom"
	}

	query := "SELECT COUNT(*) FROM " + tableName + ";"

	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		panic("[CountTrees]: " + err.Error())
	}
	var count int
	for rows.Next() {
		if err := rows.Scan(&count); err != nil {
			log.Fatal("[CountTrees]: ", err)
		}
	}
	return strconv.Itoa(count)
}

// ValidateTree will get tree from api_strom, add it to strom and remove from api_strom
func ValidateTree(id int) (newID int, err error) {
	treeRow := TreeByID(id, false)
	newID, err = AddTreeRow(treeRow, true)
	if err != nil {
		return 0, err
	}

	err = DeleteTree(id)
	if err != nil {
		return 0, err
	}

	return newID, nil
}

func DeleteTree(id int) error {
	query := "DELETE FROM app_strom WHERE strom_id=" + strconv.Itoa(id) + ";"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	return err
}
