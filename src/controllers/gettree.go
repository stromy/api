package controllers

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/stromy/api.git/src/lib"

	"gitlab.com/stromy/api.git/src/database"
	"gitlab.com/stromy/api.git/src/models"
)

// TreeByID will reach to database and return tree in
func TreeByID(id int, getValide bool) (treeRow models.TreeRow) {
	var tableName string
	if getValide {
		tableName = "strom"
	} else {
		tableName = "app_strom"
	}

	query := "SELECT * FROM " + tableName + " WHERE strom_id=" + strconv.Itoa(id) + ";"

	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		panic("[TreeById]: " + err.Error())
	}
	treeRow = scanRowsForTrees(rows, getValide)[0]

	XF, err := strconv.ParseFloat(treeRow.Lokal.X, 64)
	if err != nil {
		log.Println(err)
	}
	YF, err := strconv.ParseFloat(treeRow.Lokal.Y, 64)
	if err != nil {
		log.Println(err)
	}
	LAT, LON := lib.JTSKtoWGS84(-XF, -YF)
	treeRow.Lokal.Lon = fmt.Sprintf("%f", LON)
	treeRow.Lokal.Lat = fmt.Sprintf("%f", LAT)

	return treeRow
}

// GetTrees reaches to dtb and get n trees from start
func GetTrees(
	start string, n string, getValide bool,
	vlast []string, typObj []string, center []string, radius string,
	dateStart string, dateEnd string,
	krajID []string, kodokID []string, orpID []string, mZchuID []string, vZchuID []string) []models.TreeRow {
	if start == "" {
		start = "0"
	}
	if n == "" {
		n = "20"
	}

	// get table name
	var tableName string
	if getValide {
		tableName = "strom"
	} else {
		tableName = "app_strom"
	}
	whereQuery := " WHERE "

	// VLAST
	if len(vlast) > 0 {
		vlastQuery := ""
		for _, v := range vlast {
			vlastQuery += "'" + v + "',"
		}
		whereQuery += "VLAST in (" + vlastQuery[:len(vlastQuery)-1] + ") AND "
	}

	// TYP OBJ
	if len(typObj) > 0 {
		typObjQuery := ""
		for _, t := range typObj {
			typObjQuery += "'" + t + "',"
		}
		whereQuery += "TYP_OBJ in (" + typObjQuery[:len(typObjQuery)-1] + ") AND "
	}

	// COORDINATES BETWEEN
	if len(center) == 2 && center[0] != "" && center[1] != "" && radius != "" {
		whereQuery += processRadiusQ(center, radius)
	}

	// DATE IN INTERVAL
	if dateStart != "" && dateEnd != "" {
		dateQuery := "DATIN BETWEEN '" + dateStart + "' AND '" + dateEnd + "' AND "
		whereQuery += dateQuery
	}

	// KRAJ, KODOK, ORP, MZCHU, VZCHU
	whereQuery += processAreaQ("KRAJ", krajID)
	whereQuery += processAreaQ("KODOK", kodokID)
	whereQuery += processAreaQ("ORP", orpID)
	whereQuery += processAreaQ("M_ZCHU", mZchuID)
	whereQuery += processAreaQ("V_ZCHU", vZchuID)

	whereQuery = whereQuery[:len(whereQuery)-4] // remove AND at the end
	if len(whereQuery) < 10 {
		whereQuery = ""
	}
	// construct query
	query := "SELECT * FROM " + tableName + " " + whereQuery + " LIMIT " + start + "," + n + ";"
	rows, err := database.DtbConnection.Query(query)
	defer rows.Close()
	if err != nil {
		log.Println("[GetNTrees]: ", err)
	}
	return scanRowsForTrees(rows, getValide)
}

func processAreaQ(colName string, areaID []string) string {
	if len(areaID) > 0 {
		areaQuery := ""
		for _, aID := range areaID {
			areaQuery += "'" + aID + "',"
		}
		return colName + " IN (" + areaQuery[:len(areaQuery)-1] + ")) AND "
	}
	return ""
}

func processRadiusQ(center []string, radius string) string {
	LON, err := strconv.ParseFloat(center[0], 64)
	if err != nil {
		return ""
	}
	LAT, err := strconv.ParseFloat(center[1], 64)
	if err != nil {
		return ""
	}

	X, Y := lib.WGS84toJTSK(LAT, LON)
	r, err := strconv.ParseFloat(radius, 64)
	if err != nil {
		return ""
	}
	if X == 0 || Y == 0 || r == 0 {
		return ""
	}

	return "SQRT( POW(" + fmt.Sprintf("%.4f", X) + " - lokal.X, 2) + POW(" + fmt.Sprintf("%.4f", Y) + "-lokal.Y, 2) ) <= " + fmt.Sprintf("%.4f", r) + ") AND "
}

func scanRowsForTrees(rows *sql.Rows, getValide bool) (trees []models.TreeRow) {

	// strom, pisemne_d, obrazove_d, ohro, lokal, kateg, comment
	for rows.Next() {
		var stromID sql.NullInt64
		var NAME, IDEX, typObj, DATIN, DATAK, DATVY, DATCS, VLAST, EXURL, X, Y, ORP, KODOK, KRAJ, mZchu, vZchu, URL_PD, URL_OD, KATEG1, KATEG2, KATEG3, KATEG4, KATEG5, comU, comA, OHRO1, OHRO2, OHRO3, OHRO4, OHRO5 sql.NullString
		err := rows.Scan(
			&stromID, &IDEX, &NAME, &typObj, &DATIN, &DATAK, &DATVY, &DATCS, &VLAST, &EXURL,
			&X, &Y, &ORP, &KODOK, &KRAJ, &mZchu, &vZchu,
			&URL_PD,
			&URL_OD,
			&KATEG1, &KATEG2, &KATEG3, &KATEG4, &KATEG5,
			&OHRO1, &OHRO2, &OHRO3, &OHRO4, &OHRO5,
			&comU, &comA)
		if err != nil {
			panic("[scanRowsForTrees]: " + err.Error())
		}
		var tree models.Tree
		tree.FillStruct(stromID.Int64, IDEX.String, NAME.String, typObj.String, DATIN.String, DATAK.String, DATVY.String, DATCS.String, VLAST.String, EXURL.String)
		fmt.Println(X, Y)
		var lokal models.Lokal
		lokal.FillStruct(X.String, Y.String, "", "", ORP.String, KODOK.String, KRAJ.String, mZchu.String, vZchu.String)
		var pd models.PisemneD
		pd.FillStruct(URL_PD.String)
		var od models.ObrazoveD
		od.FillStruct(URL_OD.String)
		var kateg models.Kateg
		kateg.FillStruct(KATEG1.String, KATEG2.String, KATEG3.String, KATEG4.String, KATEG5.String)
		var comment models.Comment
		comment.FillStruct(comU.String, comA.String)
		var ohro models.Ohro
		ohro.FillStruct(OHRO1.String, OHRO2.String, OHRO3.String, OHRO4.String, OHRO5.String)

		var treeRow models.TreeRow
		treeRow.Tree = tree
		treeRow.Lokal = lokal
		treeRow.PisemneD = pd
		treeRow.ObrazoveD = od
		treeRow.Kateg = kateg
		treeRow.Comment = comment
		treeRow.Ohro = ohro
		trees = append(trees, treeRow)
	}
	return trees
}
