module gitlab.com/stromy/api.git

go 1.14

require (
	github.com/encero/sjtsk2gps v0.0.0-20190109134857-30b9742450b4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/srinathgs/mysqlstore v0.0.0-20200417050510-9cbb9420fc4c
	golang.org/x/crypto v0.0.0-20201002094018-c90954cbb977
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)
